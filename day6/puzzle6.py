
_debug = False

def puzzle1():
    with open('input.txt', 'r') as f:
        groups = (f.read()).split('\n\n')

    yes_sum = 0
    for group in groups:
        group = group.replace('\n', '')
        group_set = set(list(group))
        yes_sum += len(group_set)
    print(yes_sum)


def puzzle2():
    with open('input.txt', 'r') as f:
        groups = (f.read()).split('\n\n')

    yes_sum = 0
    for group in groups:
        answers = group.split('\n')
        ## this is annoying 
        answers = list(filter(lambda a: a != '', answers))
        intersect = set(list(answers[0].rstrip()))
        if _debug: print('initial set', intersect)
        if _debug: print(answers)
        for answer in answers:
            if _debug: print('ans', answer, set(list(answer)))
            intersect = intersect.intersection(set(list(answer.rstrip())))
            if _debug: print('ans intersect', intersect)
            if _debug: print('end set', len(intersect), intersect)
        yes_sum += len(intersect)
    print(yes_sum)


if __name__ == '__main__':

#    puzzle1()
    puzzle2()



