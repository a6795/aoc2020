import argparse

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def parse_input(contents):
    
    # rules
    rules = dict()
    idx = 0
    while 'your ticket' not in contents[idx]:
        if len(contents[idx].rstrip()) > 0:
            tokens = contents[idx].rstrip().split(':')
            rules[tokens[0]] = set() 
            rules_ranges = tokens[1].split('or')
            for rules_range in rules_ranges:
                vals = rules_range.split('-')
                rules[tokens[0]].update([ _ for _ in range(int(vals[0]), int(vals[1]) + 1) ])
        idx += 1
    if _debug: print("rules:", rules)

    # your ticket
    idx += 1
    your_ticket_vals = [ int(v) for v in contents[idx].rstrip().split(',') ]
    idx += 3
    if _debug: print("your ticket:", your_ticket_vals)

    # nearby tickets
    nearby_tickets = [] 
    while idx < len(contents):
        nearby_ticket_vals = [ int(v) for v in contents[idx].rstrip().split(',') ]
        nearby_tickets.append(nearby_ticket_vals)
        idx += 1
    if _debug: print("nearby tickets:", nearby_tickets)

    return (rules, your_ticket_vals, nearby_tickets)


def puzzle1(contents):
    rules, your_ticket, nearby_tickets = parse_input(contents)

    ranges = set()
    for k, v in rules.items():
        ranges.update(v)
    if _debug: print("ranges set:", ranges)

    sum_invalid = 0
    for ticket in nearby_tickets:
        for val in ticket:
            if val not in ranges:
                sum_invalid += val

    print(sum_invalid)

def puzzle2(contents):
    rules, your_ticket, nearby_tickets = parse_input(contents)

    ranges = set()
    for k, v in rules.items():
        ranges.update(v)
    if _debug: print("ranges set:", ranges)

    valid_tickets = [] 
    for ticket in nearby_tickets:
        valid = True
        for val in ticket:
            if val not in ranges:
                valid = False
        if valid:
            valid_tickets.append(ticket)
    if _debug: print("valid tickets:", valid_tickets)

    valid_tickets.append(your_ticket)

    ticket_format = [[] for _ in range(len(your_ticket))]
    for idx in range(len(your_ticket)):
        possibilities = list(rules.keys())
        for k, v in rules.items():
            for ticket in valid_tickets:
                if ticket[idx] not in rules[k]:
                    possibilities.remove(k)
                    break
        ticket_format[idx] = possibilities
    if _debug: print("possible ticket formats:", ticket_format)

    filtered = False
    while not filtered:
        filtered = True
        ## find elems of len 1:
        for idx in range(len(ticket_format)):
            if len(ticket_format[idx]) == 1:
                if _debug: print("to remove:", ticket_format[idx][0])
                ## got through everything else and 
                ## remove this element
                for _idx in range(len(ticket_format)):
                    if _idx != idx:
                        if ticket_format[idx][0] in ticket_format[_idx]:
                            ticket_format[_idx].remove(ticket_format[idx][0])
                            filtered = False
                if _debug: print(ticket_format)
    final_ticket_format = [ v[0] for v in ticket_format ]
    if _debug: print("final ticket formats:", final_ticket_format)

    mult = 1
    for idx in range(len(final_ticket_format)):
        if final_ticket_format[idx].startswith('departure'):
            mult *= your_ticket[idx]

    print(mult)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    # puzzle1(contents)
    puzzle2(contents)
