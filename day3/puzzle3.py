import numpy as np
import math


def create_map(col_offset):
    with open('input.txt', 'r') as f:
        contents = (f.read()).split('\n')
    print(f'{len(contents)} rows')
    repeats = math.ceil(len(contents)/len(contents[0])) * col_offset
    world_map = np.zeros(shape=(len(contents), len(contents[0]) * repeats))
    for _idx in range(len(contents)-1):
        row = [ 0 if _ == '.' else 1 for _ in contents[_idx] ]
        world_row = np.tile(row, repeats)
        world_map[_idx] = world_row
    return world_map


def puzzle1(world_map, step, right):
    trees_count = 0
    _c = right
    for _r in range(1, len(world_map), step):
        if world_map[_r][_c]:
            trees_count += 1
        _c += right
    return trees_count


def puzzle1_v2(world_map, down_step, right_step):
    row_len = len(world_map[0])
    #print(f'row len: {row_len}')
    trees = 0
    _r = right_step
    for _d in range(down_step, len(world_map)-1, down_step):
#        print(f'd: {_d} r: {_r}')
        if world_map[_d][_r] == '#':
            trees += 1
        _r = (_r + right_step) % row_len
    return trees


def puzzle2_v2(world_map, slopes):
    trees_factor = 1
    for (down, right) in slopes:
        trees_count = puzzle1_v2(world_map, down, right)
        print(f'{down} {right}: {trees_count}')
        trees_factor *= trees_count
    return trees_factor


def puzzle2(slopes):
    max_right = 0
    for (down, right) in slopes:
        if right > max_right:
            max_right = right
    world_map = create_map(max_right)
    trees_factor = 1
    for (down, right) in slopes:
        trees_count = puzzle1(world_map, down, right)
        print(f'{down} {right}: {trees_count}')
        trees_factor *= trees_count
    return trees_factor


if __name__ == '__main__':

    slopes = [(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)]
    ## for puzzle 1:
    #world_map = create_map(3)
    #print(puzzle1(world_map, 1, 3))

    ## for puzzle 2:
    ## (down, right)
    #print(puzzle2(slopes))

    ## v2
    with open('input.txt', 'r') as f:
        world_map = (f.read()).split('\n')

    ## puzzle 1
    print(puzzle1_v2(world_map, 1, 3))

    ## puzzle 2
    print(puzzle2_v2(world_map, slopes))
