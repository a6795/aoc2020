import argparse
import re


_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def parse_no_paranth(elems):
    eval_eq = 0
    operator = None
    for elem in elems:
        if elem.isnumeric():
            if operator is None:
                eval_eq = int(elem)
            else:
                if operator == '+':
                    eval_eq += int(elem)
                elif operator == '*':
                    eval_eq *= int(elem)
        else:
            operator = elem
    return eval_eq


def parse_no_paranth_2(elems):
    eval_eq = 0
    operator = None
    
    new_elems = []

    ## do addition first
    pluses = True

    while pluses:
        idx = 0
        new_elems = []
        pluses = False
        while idx < len(elems):
            if elems[idx] == '+': 
                op_one = new_elems.pop()
                op_two = elems[idx + 1]
                new_elem = int(op_one) + int(op_two)
                new_elems.append(new_elem)
                idx += 1
                pluses = True
            else:
                new_elems.append(elems[idx])
            idx += 1
        elems = list(new_elems)

    if _debug: print("finished pluses") 

    while '*' in elems:
        new_elems = []
        idx = 0
        while idx < len(elems):
            if elems[idx] == '*':
                op_one = new_elems.pop()
                op_two = elems[idx + 1]
                new_elem = int(op_one) * int(op_two)
                new_elems.append(new_elem)
                idx += 1
            else:
                new_elems.append(elems[idx])
            idx += 1
        elems = list(new_elems)

    if _debug: print("ans:", elems[0])
    return elems[0] 


def parse_eq(eq, variant = 1):
    if _debug: print("**", eq)
    clear = False
    while not clear:
        clear = True 
        groups = re.search(r'\(\s*(\d+\s(\*|\+)\s(\d+)?)*\s*\)', eq)
        if not groups is None:
            clear = False
            if _debug: print(groups.group(), groups.span())
            elems = groups.group()[1:-1].split(' ')
            if variant == 1:
                ans = parse_no_paranth(elems)
            else:
                ans = parse_no_paranth_2(elems)
            paranth_start, paranth_end = groups.span()
            eq = eq[:paranth_start] + str(ans) + eq[paranth_end:]
            if _debug: print('*', eq)
    
    if _debug: print("last eq:", eq)
    elems = eq.split(' ')
    if variant == 1:
        ans = parse_no_paranth(elems)
    else:
        ans = parse_no_paranth_2(elems)
    if _debug: print('ans:', ans)
    return ans

    
def puzzle1(contents):
    sum_eq = 0
 
    for line in contents:
        line = line.rstrip()
        sum_eq += parse_eq(line)
    
    print("sum of equations:", sum_eq)


def puzzle2(contents):
    sum_eq = 0
 
    for line in contents:
        line = line.rstrip()
        sum_eq += parse_eq(line, 2)
    
    print("sum of equations:", sum_eq)



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    puzzle1(contents)
    puzzle2(contents)
