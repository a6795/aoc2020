import argparse

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.read()
    return contents.rstrip()


def puzzle1(contents, target_iter=10):
    start_numbers = [ int(i) for i in contents.split(',') ]
    numbers = dict()

    for idx in range(len(start_numbers) - 1):
        numbers[start_numbers[idx]] = idx + 1


    iteration = len(start_numbers) + 1
    last_nbr = start_numbers.pop()

    while iteration <= target_iter:
        if _debug: print("iteration", iteration)
        if last_nbr not in numbers:
            output = 0
            if _debug: print("ln:", last_nbr, "o:", output)
        else:
            prev_occ = numbers[last_nbr]
            output = iteration - 1 - prev_occ
            if _debug: print("ln:", last_nbr, "o:", output, 'last pos', prev_occ)
        numbers[last_nbr] = iteration - 1
        last_nbr = output

        iteration += 1

    print(output)


def puzzle2(contents):
    puzzle1(contents, 30000000)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    # puzzle1(contents, 2020)
    puzzle2(contents)
