

def two_sum(numbers, target):
    for _i in range(len(numbers)):
        _err = False
        _d = target - numbers[_i]
        try:
            _idx = numbers.index(_d)
        except ValueError:
            _err = True
        if not _err:
            print(numbers[_i] * numbers[_idx])
            return (True, numbers[_i], numbers[_idx])
    return (False, 0, 0)

        #for _j in range(_i+1, len(numbers)):
        #    if numbers[_i] + numbers[_j] == target:
        #        print(numbers[_i] * numbers[_j])


def three_sum(numbers, target):
    for _i in range(len(numbers)):
        _d = target - numbers[_i]
        success, _j, _k = two_sum(numbers[_i:], _d)
        if success:
            print(numbers[_i] * _j * _k)


        #for _j in range(_i+1, len(numbers)):
        #    for _k in range(_j+1, len(numbers)):
        #        if numbers[_i] + numbers[_j] + numbers[_k] == target:
        #            print(numbers[_i] * numbers[_j] * numbers[_k])



if __name__ == '__main__':

    numbers = []
    with open('input1.txt', 'r') as f:
        line = f.readline()
        while line:
            numbers.append(int(line))
            line = f.readline()

    sorted_numbers = sorted(numbers)
    restricted_numbers = [ _ for _ in sorted_numbers if _ <= 2020 ]
    print(f'full: {len(sorted_numbers)}; restricted: {len(restricted_numbers)}')

#    two_sum(restricted_numbers, 2020)
    three_sum(restricted_numbers, 2020)

