import argparse

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def puzzle1(contents):
    adapters = [ int(a) for a in contents ]
    target = max(adapters)
    diffs = {1: 0, 2: 0, 3: 0}
    diffs_order = []

    adapters.sort()
    if _debug: print(adapters)

    current = 0
    order = []
    while len(adapters) > 0:
        adapter = adapters.pop(0)
        if _debug: print("current:", current, "adapter:", adapter)
        if adapter - 3 <= current < adapter:
            diffs_order.append(adapter-current)
            diffs[adapter-current] += 1
            current = adapter
            order.append(adapter)
        else:
            print("Cannot find next adapter!")
            break
    diffs[3] += 1
    if _debug: print(diffs)
    print("puzzle1:", diffs[1] * diffs[3])
    return (order, target + 3, diffs_order) 
   

def puzzle2(contents):
    order, device_jolts, diffs_order = puzzle1(contents)

    if _debug: print("diffs order:", diffs_order)

    # find longest sequence of ones in diffs_order
    max_ones = 0
    current_sequence_len = 0
    for diff in diffs_order:
        if diff == 3:
            if max_ones < current_sequence_len:
                max_ones = current_sequence_len
            current_sequence_len = 0
        else:
            current_sequence_len += 1

    if _debug: print(max_ones)

    # create dictionary of diffs to options
    diffs_map = {1: 1, 2: 2, 3: 4}
    for _i in range(4, max_ones+1):
        diffs_map[_i] = diffs_map[_i-1] + diffs_map[_i-2] + diffs_map[_i-3]
    
    # find sequences of ones and map them
    possibilities = 1
    current_sequence_len = 0
    for diff in diffs_order:
        if diff == 3:
            if current_sequence_len > 0:
                if _debug: print("seq:", current_sequence_len, "p:", diffs_map[current_sequence_len])
                possibilities *= diffs_map[current_sequence_len] 
            current_sequence_len = 0
        else:
            current_sequence_len += 1
    if current_sequence_len > 0:
        possibilities *= diffs_map[current_sequence_len] 
    print("possibilities:", possibilities)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    #puzzle1(contents)
    puzzle2(contents)
