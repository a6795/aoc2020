import argparse

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def get_seat_map(contents):
    seat_map = []
    for line in contents:
        line = list(line.rstrip())
        seat_map.append(line)

    ## border it with floor
    border = ['.' for _ in range(len(seat_map[0]) + 2)]
    for row in range(len(seat_map)):
        seat_map[row].insert(0, '.')
        seat_map[row].append('.')
    seat_map.insert(0, border)
    seat_map.append(border)

    return seat_map


def adjacent_seats_status(seat_map, row, col):
    adjacent_seats = seat_map[row - 1][col - 1:col + 2] if col < len(seat_map[row]) - 1 else seat_map[row - 1][col - 1:] 
    adjacent_seats.extend(seat_map[row + 1][col - 1:col + 2] if col < len(seat_map[row]) - 1 else seat_map[row + 1][col - 1:]) 
    adjacent_seats.append(seat_map[row][col - 1])
    adjacent_seats.append(seat_map[row][col + 1])
    empty = adjacent_seats.count('L')
    full = adjacent_seats.count('#')
    floor = adjacent_seats.count('.')

    return (empty, full, floor)


def visible_seats_status(seat_map, row, col):
    if _debug: print("target:", row, col)
    seats = {}
    # north 
    n = row - 1
    while n > 0:
        if not seat_map[n][col] == '.':
            break
        n -= 1
    seats['n'] = (n, col)

    # south
    s = row + 1
    while s < len(seat_map) - 1:
        if not seat_map[s][col] == '.':
            break
        s += 1
    seats['s'] = (s, col)

    # west
    w = col - 1
    while w > 0:
        if not seat_map[row][w] == '.':
            break
        w -= 1
    seats['w'] = (row, w)
 
    # east 
    e = col + 1
    while e < len(seat_map[row]) - 1:
        if not seat_map[row][e] == '.':
            break
        e += 1
    seats['e'] = (row, e)

    # north east
    n = row - 1
    e = col + 1
    while n > 0 and e < len(seat_map[row]) - 1:
        if not seat_map[n][e] == '.':
            break
        n -= 1
        e += 1
    seats['ne'] = (n, e)

    # north west
    n = row -1
    w = col - 1
    while n > 0 and w > 0:
        if not seat_map[n][w] == '.':
            break
        n -= 1
        w -= 1
    seats['nw'] = (n, w)

    # south east
    s = row + 1
    e = col + 1
    while s < len(seat_map) - 1 and e < len(seat_map[row]) -1:
        if not seat_map[s][e] == '.':
            break
        s += 1
        e += 1
    seats['se'] = (s, e)

    # south west
    s = row + 1
    w = col -1
    while s < len(seat_map) - 1 and w > 0:
        if not seat_map[s][w] == '.':
            break
        s += 1
        w -= 1
    seats['sw'] = (s, w)

    empty = 0
    full = 0
    floor = 0
    for k, (r, c) in seats.items():
        if seat_map[r][c] == '.':
            floor += 1
        if seat_map[r][c] == 'L':
            empty += 1
        if seat_map[r][c] == '#':
            full += 1
    
    if _debug: print(seats)

    return (empty, full, floor)


def apply_rules(seat_map):
    changes = 0
    new_map = []
    border = ['.' for _ in range(len(seat_map[0]))]
    new_map.append(border)
    for row in range(1, len(seat_map) - 1):
        new_row = ['.']
        for col in range(1, len(seat_map[row]) - 1):
            # get empty seats around:
            if not seat_map[row][col] == '.':
                empty, full, floor = adjacent_seats_status(seat_map, row, col)
                if _debug: print("seat:", seat_map[row][col], "empty:", empty, \
                    "full:", full, "floor:", floor)
                if seat_map[row][col] == 'L' and  full == 0:
                    new_row.append('#')
                    changes += 1
                    if _debug: print("fill seat")
                elif seat_map[row][col] == '#' and  full >= 4:
                    new_row.append('L')
                    changes += 1
                    if _debug: print("empty seat")
                else:
                    new_row.append(seat_map[row][col])
            else:
                new_row.append('.')
        new_row.append('.')
        new_map.append(new_row)
    new_map.append(border)
    return (new_map, changes)


def apply_rules2(seat_map):
    changes = 0
    new_map = []
    border = ['.' for _ in range(len(seat_map[0]))]
    new_map.append(border)
    for row in range(1, len(seat_map) - 1):
        new_row = ['.']
        for col in range(1, len(seat_map[row]) - 1):
            # get empty seats around:
            if not seat_map[row][col] == '.':
                empty, full, floor = visible_seats_status(seat_map, row, col)
                if _debug: print("seat:", seat_map[row][col], "empty:", empty, \
                    "full:", full, "floor:", floor)
                if seat_map[row][col] == 'L' and  full == 0:
                    new_row.append('#')
                    changes += 1
                    if _debug: print("fill seat")
                elif seat_map[row][col] == '#' and  full >= 5:
                    new_row.append('L')
                    changes += 1
                    if _debug: print("empty seat")
                else:
                    new_row.append(seat_map[row][col])
            else:
                new_row.append('.')
        new_row.append('.')
        new_map.append(new_row)
    new_map.append(border)
    return (new_map, changes)


def print_map(seat_map):
    for row in seat_map:
        row_str = ' '.join(row)
        print(row_str)


def puzzle1(contents):
    # . -- floor; L -- free seat; # -- empty seat
    seat_map = get_seat_map(contents)
    print_map(seat_map) 
    print('*' * 20)

    changes = 1
    while changes != 0:
        seat_map, changes = apply_rules(seat_map)
        print_map(seat_map)
        print('*' * 20)

    print("Final map:")
    print_map(seat_map)

    occupied = 0
    for row in seat_map:
        occupied += row.count('#')
    
    print("Occupied Seats:", occupied)

def puzzle2(contents):
    # . -- floor; L -- free seat; # -- empty seat
    seat_map = get_seat_map(contents)
    print_map(seat_map) 
    print('*' * 20)

    changes = 1
    while changes != 0:
        seat_map, changes = apply_rules2(seat_map)
        print_map(seat_map)
        print('*' * 20)
    # for i in range(3):
    #     seat_map, changes = apply_rules2(seat_map)
    #     print_map(seat_map)
    #     print('*' * 20)

    print("Final map:")
    print_map(seat_map)

    occupied = 0
    for row in seat_map:
        occupied += row.count('#')
    
    print("Occupied Seats:", occupied)



if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    # puzzle1(contents)
    puzzle2(contents)
