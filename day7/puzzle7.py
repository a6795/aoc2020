from igraph import Graph, plot, summary
import re

_debug = False


def create_graph(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()

    print(len(contents))

    g = Graph(directed=True)
    g.add_vertices(len(contents))

    rg = re.compile(r'^\w+ \w+(?=\s+bag)')
    for _idx in range(len(contents)):
        cols = rg.findall(contents[_idx])
        if _debug: print(cols)
        start_col = cols.pop(0)
        g.vs[_idx]['name'] = start_col

    summary(g)

    rg = re.compile(r'\w+ \w+(?=\s+bag)')
    for line in contents:
        cols = rg.findall(line)
        if _debug: print(cols)
        start_col = cols.pop(0)
        parent_vs = g.vs.find(name = start_col)
        for _idx in range(len(cols)):
            if cols[_idx] != 'no other':
                child_vs = g.vs.find(name = cols[_idx])
                es = g.add_edge(parent_vs.index, child_vs.index)
                # get the number
                p = re.compile(r'(\d+) ' + r'{}'.format(cols[_idx]))
                nbr = p.findall(line).pop(0)
                es['nbr'] = int(nbr)
                if _debug: print(nbr, cols[_idx])

    summary(g)
    return g


def get_all_neighbours(g, node_id, neighbours, mode):
    neighbs = g.neighbors(g.vs[node_id], mode=mode)
    if len(neighbs) > 0:
        neighbours.update(neighbs)
        for _idx in range(len(neighbs)):
            get_all_neighbours(g, neighbs[_idx], neighbours, mode)


def bags_FFS(g, start_node):

    bags_queue = [start_node.index]
    bags_sum = 0
    while len(bags_queue) > 0:
        if _debug: print([ g.vs[idx]['name'] for idx in bags_queue ])
        node_id = bags_queue.pop(0)
        bags_sum += 1
        children = g.neighbors(g.vs[node_id], mode='OUT')
        for child in children:
            edge = g.get_eid(node_id, child, directed=False)
            bags_queue.extend([ child for _ in range(g.es[edge]['nbr']) ])
    return (bags_sum - 1)


def print_graph(g):
    layout = g.layout("fr")
    plot_style = {}
    plot_style["layout"] = layout
    plot_style["bbox"] = (800, 1000)
    plot_style["margin"] = 60
    plot_style["vertex_label"] = [ v['name'] + ' ' + str(v['bags']) for v in  g.vs ]
    plot_style["edge_label"] = [ str(e.index) + ' ' + str(e['nbr']) for e in g.es ]
    plot(g, "graph.png", **plot_style)


def puzzle1(colour):
    g = create_graph()
    target_vs = g.vs.find(name = colour)

    parents_set = set()
    get_all_neighbours(g, target_vs.index, parents_set, 'IN')
    print(parents_set)
    return len(parents_set)


def puzzle2(colour):
    g = create_graph('input_v2.txt')

    target_vs = g.vs.find(name = colour)
    return bags_FFS(g, target_vs)


if __name__ == '__main__':
    #print(puzzle1('shiny gold'))
    print(puzzle2('shiny gold'))
