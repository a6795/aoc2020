import math


def get_number(tag, low_letter, high_letter, lower, upper):
    for _letter in tag:
        if _letter == low_letter:
            upper = lower + math.floor((upper-lower)/2)
        if _letter == high_letter:
            lower = lower + math.ceil((upper-lower)/2)
        #print(f'{_letter}, {lower:2}, {upper:2}')
    return lower


def get_id(tag):
    row_tag = tag[:7]
    seat_tag = tag[7:]
    row = get_number(row_tag, 'F', 'B', 0, 127)
    #print('row', row)
    seat = get_number(seat_tag, 'L', 'R', 0, 7)
    #print('seat', seat)

    return row * 8 + seat


def puzzle1():
    max_id = 0
    with open('input.txt', 'r') as f:
        line = f.readline()
        while line:
            seat_id = get_id(line.strip())
            if seat_id > max_id:
                max_id = seat_id
            line = f.readline()
    print(max_id)


def puzzle2():
    ids = []
    with open('input.txt', 'r') as f:
        line = f.readline()
        while line:
            seat_id = get_id(line.strip())
            ids.append(seat_id)
            line = f.readline()
    ids.sort()
    for _idx in range(len(ids)-1):
        if ids[_idx] + 2 == ids[_idx + 1]:
            print(ids[_idx] + 1)


if __name__ == '__main__':

#    puzzle1()
    puzzle2()
