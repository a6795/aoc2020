
_debug = True


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents


def puzzle1(contents):
    acc = 0
    pc = 0
    visited = [ 0 for _ in range(len(contents)) ]
    while pc < len(contents) and visited[pc] != 1:
        instr = contents[pc][:3]
        op = int(contents[pc][4:])
        visited[pc] = 1
        if instr == 'acc':
            acc += op
            pc += 1
        elif instr == 'jmp':
            pc += op
        else:
            pc += 1
    print("accumulator:", acc)
    return(acc, pc)


def puzzle2(contents):
    # create new versions, iteratively:
    end_pc = len(contents)
    print(end_pc)
    for idx in range(len(contents)):
        new_contents = list(contents)
        instr = contents[idx][:3]
        if instr == 'nop':
            if _debug: print("changed a nop")
            new_contents[idx] = new_contents[idx].replace('nop', 'jmp')
        elif instr == 'jmp':
            if _debug: print("changed a jmp")
            new_contents[idx] = new_contents[idx].replace('jmp', 'nop')
        else:
            continue
        (acc, pc) = puzzle1(new_contents)
        if pc >= end_pc:
            print("Succcess", acc)
            return 0
        else:
            print("Still loops", pc)



if __name__ == '__main__':
    contents = read_input('input.txt')
    #puzzle1(contents)
    puzzle2(contents)
