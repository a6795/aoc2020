import argparse

_debug = False


def read_input(filename):
    with open(filename, 'r') as f:
        contents = f.readlines()
    return contents



def puzzle1(contents):

    mem = dict()
    for line in contents:
        if 'mask' in line:
            mask = line[7:].rstrip()
            if _debug: print(f"mask: {mask}")
        else:
            end_idx = line.find(']')
            idx = int(line[4:end_idx])
            if idx not in mem:
                mem[idx] = ''
            val = int(line[end_idx+4:])
            mem[idx] = list(f'{val:036b}')
            if _debug: print(idx, f'{val:036b}')

            for _idx in range(len(mask)):
                if mask[_idx] == '1':
                    mem[idx][_idx] = '1'
                elif mask[_idx] == '0':
                    mem[idx][_idx] = '0'
            if _debug: print("new val:", mem[idx])

    sum_mem = 0
    for k, v in mem.items():
        sum_mem += int(''.join(v),2)
    print("sum:", sum_mem)


def puzzle2(contents):
    mem = dict()
    sum = 0
    for line in contents:
        if 'mask' in line:
            mask = line[7:].rstrip()
            if _debug: print(f"mask: {mask}")
        else: 
            end_idx = line.find(']')
            idx = int(line[4:end_idx])
            idx_bitstring = f'{idx:036b}'
            if idx not in mem:
                mem[idx] = 0
            val = int(line[end_idx+4:])
            addr_masked = list(idx_bitstring)

            for _idx in range(len(mask)):
                if mask[_idx] == '1':
                    addr_masked[_idx] = '1'
                elif mask[_idx] == '0':
                    addr_masked[_idx] = idx_bitstring[_idx]
                else:
                    addr_masked[_idx] = 'X'
            
            xes = addr_masked.count('X')
            addrs = [ ] 
            
            x_masks = [ f'{_:b}' for _ in range(2 ** xes) ]
            if _debug: print("x masks:", x_masks)

            for xmask in x_masks:
                x_mask = list(xmask.zfill(xes))
                new_addr = [] 
                for bit in addr_masked:
                    if bit == 'X':
                        new_addr.append(x_mask.pop(0))
                    else:
                        new_addr.append(bit)
                    
                mem[''.join(new_addr)] = val

    sum_mem = 0
    for k, v in mem.items():
        sum_mem += v
    print(sum_mem)
 


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='AOC 2020', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", action='store_true', help="enable debug")
    args = parser.parse_args()
    if args.d:
        _debug = True

    contents = read_input('input.txt')
    # puzzle1(contents)
    puzzle2(contents)
