import re


def puzzle1():
    valid_passwords = 0
    with open('input1.txt', 'r') as f:
        line = f.readline()
        while line:
            components = re.split('\W+', line.strip())
            # 0 -- min; 1 -- max; 2 -- char; 3 -- password
            line = f.readline()
            cnt = components[3].count(components[2])
            if cnt >= int(components[0]) and cnt <= int(components[1]):
                valid_passwords += 1
    print(valid_passwords)


def puzzle2():
    valid_passwords = 0
    with open('input1.txt', 'r') as f:
        line = f.readline()
        while line:
            components = re.split('\W+', line.strip())
            # 0 -- first index; 1 -- second index; 2 -- char; 3 -- password
            line = f.readline()
            if (components[3][int(components[0])-1] == components[2]) ^ \
               (components[3][int(components[1])-1] == components[2]):
                valid_passwords += 1
        print(valid_passwords)


if __name__ == '__main__':

    puzzle2()
